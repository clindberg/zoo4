<!DOCTYPE html>
<!-- saved from url=(0022)http://www.zoozoo.com/ -->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml" style=""><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Invitation - ZooZoo.com</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile viewport optimized: h5bp.com/viewport -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

    <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
    <script async src="./stuff/analytics.js"></script><script src="./stuff/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/frontend/js/libs/jquery-1.7.1.min.js"><\/script>')</script>

        <link type="text/css" href="./stuff/10fc00d.css" rel="stylesheet">
    
    <link type="text/css" rel="stylesheet" href="//fast.fonts.com/cssapi/a83441ce-ba7e-4dfb-9178-e5688d4e31e5.css"/>

            <link rel="stylesheet" href="http://www.zoozoo.com/css/b6bc287.css">
            <link type="text/css" href="extrastyle.css" rel="stylesheet">

<?php /* <script type="text/javascript" src="stuff/jquery.js"></script> */ ?>
<script type="text/javascript" src="stuff/jquery.zclip.js"></script>
        </head>
<body class="default page" style="display:none;<?php // do asume javascript ;) ?>">
	<div class="wrapp-invitation invitation-step2">
        <header class="row header-content">
            <a class="pull-left logo" alt="Home" href="<?php echo $_SERVER['PHP_SELF']; ?>">
                <img src="./stuff/logo.png" class="pull-left">
            </a>
            <div class="pull-left slogan">- Nordens största djursajt</div>
        </header>
        <div role="main" class="invi-content">


        	<div class="invi-step2-box"><div style="width:14px;height:10px; background-color:#EEEEEE; font-size:12px; border:solid #CCCCCC 1px; cursor: pointer; color:#999; text-align:center; padding:3px; margin:2px;float:right; line-height:10px;"><a href="?logout=yep" rel="nofollow" title="Logga ut <?php echo @$_SESSION['email']; ?>| Byt email">X</a></div>
 
            
                <div class="row invi-s2-tit">Grattis till att bli en av de första på ZooZoo.com</div>
                <div class="row invi-s2-top">
                  <div class="s2-top-left">
                        <div class="s2-tl-tit"><?php if(isset($_SESSION['firstlogin'])){ echo "Hej";
						 //perhaps a more special introduktion for first time loginners; 
						$_SESSION['firstlogin'] = ""; } else { echo "Hej igen"; } ?> <?php echo $_SESSION['name']; ?>,</div>
                        <div class="s2-tl-con">
                            <p>ZooZoo.com kommer att lanseras snart. Du kommer att bli en av de första av de som får tillgång till ZooZoo.com. Håll utkik i din epost! Medan du väntar ge ditt djur lite extra kärlek och bjud in några vänner! Desto fler vänner du bjuder in, ju snabbare kommer du att få tillgång till  zoozoo.com.</p>
                            <?php if(!isset($_SESSION['email_activated'])) {?>
                            <p>Spana in din e-post för ett välkomstmeddelande samt bekräfta din e-postadress.</p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="s2-top-right hidefirst">
                      <i class="lbulb"></i>
                      <div class="s2-tr-tit">Visste du att?!</div>
                        <div class="s2-tr-con">Hundar &amp; katter lever 10-20% längre om man tar god hand om deras tänder.</div>
                    </div>
                    
                      </div>
                      
                      
                  <div class="kilian-bluebox-padding-top"></div>
                  <div class="kilian-bluebox">
                    
                    <div class="kilian-bluebox-title">Skicka e-post till dina vänner</div>
                    <div class="kilian-bluebox-undertitle">Skriv in e-postadresser och separera <br>
                    dem genom kommatecken</div>
                    
                    
                    <form method="POST" action="index.php" name="formSendInvitiedEmail" id="formSendInvitiedEmail" class="formSendInvitiedEmail">
                                <div class="row invitation-name form-item">
                                    <input type="text" id="invite_these_emails" name="invite_these_emails" required placeholder="e-mailadresser här" style="width: 164px; float:left; height:20px; margin:0px; padding:5px; border-radius:8px; margin-right: 5px;">
                                    
                                    
                                    
                                    <button type="submit" name="_submit" id="_submit" class="invi-btn-orange pull-right" style="width: 68px; font-size:14px; height:32px; float:left;  margin:0px;  padding:5px; ">Skicka</button>
                                </div>
                                
                                <input type="hidden" id="waitlist__token" name="waitlist[_token]" value="20d1b54f397b0fce90965e9f0cd023f7300f6e30">
                    </form>
                            
                    </div>
                    
                  
                  <div class="kilian-bluebox-spacer"></div>
                  <div class="kilian-bluebox">
                    
                    <div class="kilian-bluebox-title">Bjud In Dina Vänner</div>
                     <div class="kilian-bluebox-undertitle" style="height:36px;">Dela länken genom sociala medier</div>
                     
                     <?php

$fb_title=urlencode('ZooZoo.com');

$fb_url=urlencode(getmyrefurl());

$fb_summary=urlencode('ZooZoo för  djurälskare... naaaaaw.. och de som råkat få ett djur');

$fb_image=urlencode('http://www.zoozoo.com/frontend/images/logo.png');

?>
<a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $fb_title;?>&amp;p[summary]=<?php echo $fb_summary;?>&amp;p[url]=<?php echo $fb_url; ?>&amp;p[images][0]=<?php echo $fb_image;?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)">
                     <img src="soc_01.png" width="71" height="69">
                     </a>
                     
                     <a href="http://twitter.com/share?url=<?php echo getmyrefurl(); ?>&text=<?php echo urlencode("Djurälskare och de som råkat få ett djur.. Nu finns zoozoo.com! Spana in"); ?>" target="_blank" rel="nofollow">
    <img src="soc_02.png" width="69" height="69">
  </a>
      
      
      <a href="https://plus.google.com/share?url=<?php echo getmyrefurl(); ?>&title=<?php echo urlencode("ZooZoo.com - stället för djurälskare"); ?>" target="_blank" rel="nofollow">
                     <img src="soc_04.png" width="69" height="69">
      </a>
      
      
                    </div>
                     <div class="kilian-bluebox-spacer"></div>
                  <div class="kilian-bluebox">
                    
                    <div class="kilian-bluebox-title">Klistra in länken direkt</div>
                     <div class="kilian-bluebox-undertitle">Kopiera länken och klistra in <br>
                       länken där andra och se den</div>
                     
                   
                                <div class="row invitation-name form-item">
                                 
                                  <input type="text" id="shareurl" name="shareurl" value="<?php echo getmyrefurl(); ?>" style="width: 164px; float:left; height:20px; margin:0px; padding:5px; border-radius:8px; margin-right: 5px; color:#999999 !important; " onBlur="this.value = '<?php echo getmyrefurl(); ?>';" >
                                  <?php

//Detect iOS related and such because then "copy" wont work
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
// $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
// $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

//check if ios ( all android work?)
if( $iPod || $iPhone || $iPad ){
$this_is_ios = 1;
}
// if(1){
if(isset($this_is_ios)){
?>
<style>
#shareurl {
	width: <?php echo 164+68 ?>px !important;
	
	}

</style>
<?php
} else {

?> 
                                  <button type="button" name="copythis" id="copythis" class="invi-btn-orange pull-right" style="width: 68px; font-size:14px; height:32px; float:left;  margin:0px;  padding:5px; ">Kopiera</button>

<?php 
}
?>                                </div>
                                
                                
                   <script>
				   
				   $("#copythis").zclip({
path: "stuff/ZeroClipboard.swf",
copy: function(){
                     
    return $(this).prev().val();
       
                    },
                    beforeCopy:function() {
                 $("#shareurl").select();
                    },
                    afterCopy:function(){
                      
                     document.getSelection().removeAllRanges();
                    }
});


				   $("input[type='text']").on("click", function () {
   $(this).select();
});


</script>
                    
                  </div>
                  <div class="kilian-bluebox-padding-top2">
                  
                  
               <div id="showupx"></div>     
                     
                     <?php // - varför inte vinna 5000 kr eller göra en god gärning för alla djur? ?>
                  <?php
				  
				   if(@$_SESSION['email_activated'] != 1){  ?>
                    
                          <div class="info " style="font-weight:bold; font-size:20px; border-width:3px; border-color:#CDE5F8; color:#00529B; background-color:#BDE5F8; height:22px; text-align:center; margin-top: 0px !important;"><span class="" style="">Bekräfta gärna emailen. Kolla din inkorg efter ett aktiveringsmail. <a href="?resendactivation=yep_NOT_ACTIVE_WILL_FIX">[Sänd pånytt]</a></span> </div>
                      </div>    
                                
                
                    <?php } ?>
                  <?php
				  
				   if(@$_SESSION['email_activated_now_so_inform'] == 1){
					   $_SESSION['email_activated_now_so_inform'] = "";
					   ?>
                    
                          <div class="info fadeoutx2" style="font-weight:bold; font-size:20px; border-width:3px; border-color:#CDE5F8; color:#00529B; background-color:#BDE5F8; height:22px; text-align:center; "><span class="" style="">Mail aktiverad :)</span> </div>
                          
                       </div> 
                     <script type="text/javascript">
            $(document).ready(function(){
								
								
								$(".fadeoutx2").fadeIn(5000, function(){
	
	$(".fadeoutx2").fadeOut(5000);
	
	});
	
				});       
					</script>
        
                    <?php } ?>
                  
                    

                    <div class="kilian-lowbox " style="text-align:center; "><span style="font-family: Arial; font-size: 18px !important; line-height:42px; font-weight:normal;">
                    
                    
                    
                    <strong><span id="counter">14</span> vänner</strong> har registrerat sig med din länk<br>
</span>
     
     
     
     
     
     
     <div style="clear:both;"></div>
     
                    <div style="text-align:right; width:280px; font-size:11px !important; background-image:url(images/images/splashpage_02_01b_03.jpg); background-repeat:no-repeat; line-height:14px; background-position:top right; height:60px; padding-right:20px; padding-top:10px; float:left; ">
                    
                    <span style="font-weight:bold; font-style:italic; ">
                    Bli först in på ZooZoo.com<br>
                    </span>
                    <div style=" float:right; width: 140px;">
                    Bjud in 5 vänner och få tillgång till en ny unik e-handel före alla andra.
                    </div>
                    </div>
                    
                    
                    
                    
                    <div style="text-align:right; width:280px; font-size:11px !important; background-image:url(images/images/splashpage_02_01b_03.jpg); background-repeat:no-repeat; line-height:14px; background-position:top right; height:60px; padding-right:20px; padding-top:10px; float:left; ">
                    
                    <span style="font-weight:bold; font-style:italic; ">
                    Få presentkort<br>
                    </span>
                   <div style=" float:right; width: 140px;">
                    Bjud in 15 vänner och få ett presentkort värt 250 kr..
                    </div>
                    </div>
                    
                    
                    
                    
                    <div style="text-align:right; width:280px; font-size:11px !important; background-image:url(images/images/splashpage_02_01b_03.jpg); background-repeat:no-repeat; line-height:14px; background-position:top right; height:60px; padding-right:20px; padding-top:10px; float:left; ">
                    
                    <span style="font-weight:bold; font-style:italic; ">
                    Värdefullt presentkort<br>
                    </span>
                   <div style=" float:right; width: 140px;">
                    Bjud in 25 vänner och få chans att vinna presentkort värt 5000 kr.
                    </div>
                    </div>
                    
                    <div style="clear:both; height:22px;"></div>
                    <div style="background-image:url(images/prog_bar_kilian_09.jpg); height:15px; ">
                    
                    <div style="background-image:url(images/prog_bar_kilian_03.jpg); width:10px; height:15px; float:left;"></div>
                    
                    <div style="background-image:url(images/prog_bar_kilian_04.jpg); width:10px; height:15px; float:left;" id="growme"></div>
                    
                    <div style="background-image:url(images/prog_bar_kilian_06.png); background-repeat:no-repeat; width:30px; height:15px; float:left;" ></div>
                     
                     <div style="background-image:url(images/prog_bar_kilian_10.jpg); width:12px; height:15px; float:right;" ></div>
                    
                    </div>



                 
                
                 <div class="kilian-bluebox-padding-top"></div>
<br>
<br>

                    <div style="text-align:right; width:280px; font-size:11px !important; line-height:11px; height:50px; padding-right:20px; padding-top:0px; float:right; color:#999; z-index:9999;">
                    
                    *Kan ej kombineras med andra rabatter.    Gäller endast en per person.   Minimumorder på 300 kr.<br>

</div>
                    
                     </div>
                     
                                <div class="animal-car"></div>
        	</div>
           </div>
    </div>

     <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="./stuff/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/frontend/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
    <script type="text/javascript" src="./stuff/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="./stuff/bootstrap.js"></script>
    <script src="./stuff/bootstrap-typeahead.js"></script>
    <script src="./stuff/jquery.uniform.min.js"></script>
    <script src="./stuff/jquery.form.js"></script>
    
   
 
            <script type="text/javascript">
			//global js variable
			salt = 0;

			
			$(document).ready(function(){

				function showup3 (salt){
					// alert(salt);
					$("#showup1" + salt).animate({height: "78px"}, 500, 'linear', function() {
				
				
			$("#showup2" + salt).fadeIn('slow', 'linear', function() {
					
					$("#showup3" + salt).fadeIn('slow', 'linear', function() {
					
					$("#showup3" + salt).fadeOut(3100, function(){
						$("#showup3" + salt).fadeOut(1100, function(){
						
						$("#showup2" + salt).fadeOut(1100, function(){
						
						$("#showup1" + salt).animate({height: "0px"}, 700, 'linear', function() {
							
							$("#showup1" + salt).fadeOut(1100);
							
							});
						});
						});
						});
					
					}); 	
					
					}); 	
					
					
					}); 
					
					}
				
				
				
				
				
				
				
				//$('body').hide();
				$('.hidefirst').hide();
				$('#counter').text('0');
				$('body').fadeIn(2000, 'linear', function(){ 
				  $('.hidefirst').fadeIn(function(){
					  //alert(<?php echo count_my_active_refs(); ?>);
					  
						//update_numbers_vis(100);
						update_numbers_vis_up();
						
					  });
				});
				
                /*
				$("#copythis").zclip({
                    path: '/stuff/ZeroClipboard.swf',
                    copy: function(){
                        return $('#shareurl').val();
                    },
                    beforeCopy:function() {
                  
                    },
                    afterCopy:function(){
                      
                    
                    }
                });
    */
	
function update_numbers_vis_up (){		
//make ajax call to determine... 
totalregs = 100; 
/*
	$({countNum: 1}).animate({countNum: totalregs}, {
  duration: 600,
    specialEasing: {
      width: "linear",
      height: "linear"
    },
  step: function() {
    // What todo on every count
    //console.log(Math.floor(this.countNum));
	$("#counter").text(Math.floor(this.countNum));
  },
  complete: function() {
    //console.log('finished');
	$('#counter').text(this.countNum);
  }
});
*/
maxtotal = 853;
enheter = maxtotal/30;
breddpagrid = enheter * totalregs;
if(breddpagrid > maxtotal) { breddpagrid = maxtotal; 
// alert("Wow fler än 30 st!"); 
}
	//count_my_tot_refs
	$("#growme").animate({
    width:  "+=" + breddpagrid + ""
  }, 900, "linear", function() {
    // Animation complete.
	

	

update_numbers_vis_down();

  });
}



function update_numbers_vis_down (){		
//make ajax call to determine... 
totalregs = 0; 
/*
	$({countNum: 100}).animate({countNum: totalregs}, {
  duration: 1000,
    specialEasing: {
      width: "linear",
      height: "easeOutBounce"
    },
  step: function() {
    // What todo on every count
    //console.log(Math.floor(this.countNum));
	$("#counter").text(Math.floor(this.countNum));
  },
  complete: function() {
    //console.log('finished');
	$('#counter').text(this.countNum);
  }
});
*/
maxtotal = 853;
enheter = maxtotal/30;
breddpagrid = enheter * totalregs;
if(breddpagrid > maxtotal) { breddpagrid = maxtotal; 
// alert("Wow fler än 30 st!"); 
}
	//count_my_tot_refs
	$("#growme").animate({
    width:  "+1"
  }, 3000, "easeOutBounce", function() {
    // Animation complete.
	

	

update_numbers_vis(<?php echo count_my_active_refs(); ?>);

  });
}









function update_numbers_vis (totalregs){		
//make ajax call to determine... 
//totalregs = 15; 
	$({countNum: 1}).animate({countNum: totalregs}, {
  duration: 1000,
    specialEasing: {
      width: "linear",
      height: "easeOutBounce"
    },
  step: function() {
    // What todo on every count
    //console.log(Math.floor(this.countNum));
	$("#counter").text(Math.floor(this.countNum));
  },
  complete: function() {
    //console.log('finished');
	$('#counter').text(this.countNum);
  }
});

maxtotal = 853;
enheter = maxtotal/30;
breddpagrid = enheter * totalregs;
if(breddpagrid > maxtotal) { breddpagrid = maxtotal; 
// alert("Wow fler än 30 st!"); 
}
	//count_my_tot_refs
	$("#growme").animate({
    width:  "+=" + breddpagrid + ""
  }, 3000, "easeOutBounce", function() {
    // Animation complete.
	

	

  });
}




// this is the id of the submit button

$("#formSendInvitiedEmail").submit(function() {

    var url = "c1.php"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#formSendInvitiedEmail").serialize(), // serializes the form's elements.
           success: function(data)
           {
               //alert(data); // show response from the php script.
			   salt++;
			   htmlstring = '<div class="showup1" id="showup1' + salt + '"><div class="showup2" id="showup2' + salt + '"><span class="showup3" id="showup3' + salt + '">' + data + '</span></div></div>';
			   $("#showupx").append(htmlstring);
//alert(htmlstring);
				showup3(salt);
	   
           }
         });

    return false; // avoid to execute the actual submit of the form.
});




/*
                $('.formSendInvitiedEmail').on('submit', function(e) {
                    e.preventDefault();
                    var $submitForm = $(this);
                    $submitForm.ajaxSubmit ({
                        error: function(jqXHR, textStatus, errorThrown){
                        var json = $.parseJSON(jqXHR.responseText);
                            if(json.errors){
                                var elem = $(".alert .errors",$submitForm);
                                elem.html('');
                                $.each(json.errors, function(index, val){
                                    var field_label = $("[id*='"+index+"']").attr("placeholder");
                                    if(field_label){
                                        elem.append(field_label + " : " +val.toString()+"<br />");
                                    }   else    {
                                        elem.append(val.toString()+"<br />");
                                    }
                                });
                                $(".alert",$submitForm).show();
                            }
                        },
                        success: function(data) {

                        },
                        beforeSend: function() {
                            $("#_submit").attr("disabled", true);
                            $("#_submit").prepend('<i class="icon-refresh icon-spin" style="margin-right: 5px;"> ');
                            ///submitForm.find('button[type="submit"]').attr("disabled", true);
                            //$submitForm.find('button[type="submit"]').prepend('<i class="icon-refresh icon-spin" style="margin-right: 5px;"> ');
                        },
                        complete: function() {
                            $submitForm.find('button[type="submit"]').removeAttr("disabled");
                            $submitForm.find('.icon-refresh').remove();
                        }
                    });
                });
				
				*/
            });

            
        </script>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-43774738-1', 'zoozoo.com');
      ga('send', 'pageview');
    </script>
</body></html>