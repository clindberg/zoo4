<!DOCTYPE html>
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml" style=""><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>ZooZoo.com</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile viewport optimized: h5bp.com/viewport -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

    <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
    <script async src="./stuff/analytics.js"></script><script src="./stuff/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/frontend/js/libs/jquery-1.7.1.min.js"><\/script>')</script>

        <link type="text/css" href="./stuff/10fc00d.css" rel="stylesheet">
    
    <link type="text/css" rel="stylesheet" href="//fast.fonts.com/cssapi/a83441ce-ba7e-4dfb-9178-e5688d4e31e5.css"/>

            <link rel="stylesheet" href="http://www.zoozoo.com/css/b6bc287.css">
            <link type="text/css" href="extrastyle.css" rel="stylesheet">
        </head>
<body class="default page">
    <div class="wrapp-invitation invitation-step1">
        <header class="header-content">
            <a class="logo" alt="Home" href="<?php echo $_SERVER['PHP_SELF']; ?>">
                <img src="./stuff/logo.png">
            </a>
        </header>
        <div role="main" class="invi-content">
            <div class="invi-step1-box">
                <div class="step1-box-title">Kärlek, Kunskap, Kvalitet</div>
                <div class="row">
                    <div class="step1-box-left">

                        <div class="row">
                            <div class="s1-box-left-tit">
                                Favoritplats för dig och ditt djur                            </div>
                            <div class="s1-box-left-con">
                                Vi <strong>älskar</strong> djur och vill <strong>dela kärleken</strong> med alla andra djurälskare. Många roliga minnen förknippas med våra djur och dessa delas gärna med andra.<br>
                          ZooZoo vill även underlätta och göra det <strong>mer bekvämt</strong> att ha djur. </div>
                        </div>

                        <div class="row">
                            <div class="s1-box-left-tit">
                                Få ut mer än att bara handla!                            </div>
                            <div class="s1-box-left-con">
                                Med marknadens största utbud av djurprodukter samlade på ett och samma ställe kommer det nu bli ännu roligare att ha djur. Vi har gjort det överraskande lätt att handla till ditt djur och dina produkter levereras inom 48 timmar.                            </div>
                        </div>

                    </div>
                    <div class="step1-box-right">
                        <div class="s1-right-text">
                            <div class="s1-right-text-item "><i class="checkmark1"></i>Över 7.500 produkter för ditt djur</div>
                            <div class="s1-right-text-item "><i class="checkmark1"></i>Tävlingar varje månad med fina priser</div>
                            <div class="s1-right-text-item "><i class="checkmark1"></i>Helt nytt e-handelskoncept</div>
                            <div class="s1-right-text-item "><i class="checkmark1"></i>Hitta nya vänner till ditt husdjur</div>
                            <div class="s1-right-text-item "><i class="checkmark1"></i>Dela med dig &amp; ta del av andras upplevelser</div>
                        </div>
                        <?php if(@$form1valid['name'] == 1) echo "<div class='failmess'>Hmm namnet får endast vara bokstäver (som i verkligheten) med max 26 tecken</div>"; ?>
                            <?php if(@$form1valid['email'] == 1) echo "<div class='failmess'>Hmm du verkar ha skrivit emailen fel</div>"; ?>
                            
                            
                        <div class="s1-right-form">
                            <div class="s1-right-form-title">Bli bland de första 999 att få tillgång!</div>
                            
                            <form method="POST" action="index.php">
                              <div class="row invitation-name form-item">
                                    <label style="position:absolute; z-index:999; font: 0.75em/normal sans-serif; width: 47px; text-align:right; height: 12px; color: rgb(186, 186, 186); overflow: hidden; font-size-adjust: none; font-stretch: normal; right:20px; " for="waitlist_name">Namn</label><input type="text" id="waitlist_name" name="waitlist[name]"  placeholder="Namn" value="<?php echo @$_POST['waitlist']['name']; ?>" class="<?php if(@$form1valid['name'] == 1) echo "redfield"; ?>">
                                </div>
                                <div class="row invitation-email form-item">
                                    <label style="position:absolute; z-index:999; font: 0.75em/normal sans-serif; width: 47px; text-align:right; height: 12px; color: rgb(186, 186, 186); overflow: hidden; font-size-adjust: none; font-stretch: normal; right:20px; " for="waitlist_email">Email</label><input type="email" id="waitlist_email" name="waitlist[email]" required placeholder="E-post" value="<?php echo @$_POST['waitlist']['email']; ?>" class="<?php if(@$form1valid['email'] == 1) echo "redfield"; ?>">
                                </div>
                                <div class="row">
                                    <button type="submit" name="_submit" id="_submit" class="invi-btn-orange pull-right">Ja, jag vill vara först!</button>
                                </div>
                                <input type="hidden" id="waitlist__token" name="waitlist[_token]" value="20d1b54f397b0fce90965e9f0cd023f7300f6e30">
                                
                                <input type="hidden" id="ref_token" name="waitlist[ref_token]" value="<?php echo extracttoken(); // tar fram referal.. ?>">
                                
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="animal-car"></div>
        </div>
    </div>
            <script type="text/javascript">
            $(document).ready(function(){

				$(".redfield").stop().css("background-color", "#FFFF9C").animate({ backgroundColor: "#FFFFFF"}, 3500);
			});
			</script>
    <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-43774738-1', 'zoozoo.com');
      ga('send', 'pageview');
    </script>
    <div style="z-index: 999; position: absolute; bottom: 0px; margin: auto; left: 50%;">2013 Copyright ||| <a href="admin.php">[Admin]</a></div>
</body></html>