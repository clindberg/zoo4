<?php

setLocale(LC_ALL, 'sv_SE');

# run_splash_email.php

// Turn off all error reporting
// error_reporting(0);

if(!isset($_SESSION)) {
     session_start();
}

if(isset($_GET['logout'])){
	//an effetive way to unset all session variables. 
	$_SESSION = "";
	header("Location: http://". $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"],'?')); /* Redirect browser */
	/* Make sure that code below does not get executed when we redirect. */
    session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    session_regenerate_id(true);
}


if(isset($_GET['logout'])){
	header("Location: http://". $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"],'?')); /* Redirect browser */
	/* Make sure that code below does not get executed when we redirect. */
    session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    session_regenerate_id(true);
}


// The js-fonts is not fully working in chrome

// could also have set cookie
// if set the form is sent
// validate email(?) before passing 
// to step 2? Not in current online version
// make sure email is sent only after activation (ie confirming email to make sure legit use)

// register new email in db
  require_once("c1.php");


if(isset($_GET['active_email'])){
	activate_email($_GET['active_email']);
	header("Location: http://". $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"],'?') . "?"); /* Redirect browser */
	/* Make sure that code below does not get executed when we redirect. */
}


// create buffer cache, or simply send the page all at once after processing - made with a compression
//ob_start("ob_gzhandler");



if(isset($_POST["waitlist"]["email"])){
	//validate form
	$form1valid = validateform1();

	if(max($form1valid) == 1){ //  1.. = fail
		// if not ok then load step1.php and show those errors
  		require_once("step1.php");
		
		//print_r($form1valid);
	
	} else {
		//if ok register etc :) - doesnt double check.. but just updates.. could have a login (but skipped it)
		reg_new_user();
		// reload to avoid double post of form and stuff... :)
		header("Location: http://". $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"],'?')); /* Redirect browser */
	}
}

if(isset($_SESSION['email'])){
// load view
  require_once("step2.php");

} else {
  require_once("step1.php");
}

//require_once("c1.php");

//output stuff 
//ob_end_flush();



?>