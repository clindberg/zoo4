<?php

// Set default timezone
//  date_default_timezone_set('UTC');

// Notes: Preferd a more DBO-ish -solution to the database integration, but that demanded a fully feathered OOB structure

//Let's use cookies, if it's not already active
if(!isset($_SESSION)) {
     session_start();
}

// could be avoided .. but this is supposed to be a fast hack
global $db, $escapedPost, $escapedGet; 

//Semi security all escaped in one go, since sql not prepared with DBO
if (isset($_POST)) {
	//array map wont work on array in array. 
	$escapedPost = recursive_escape($_POST);
}
if (isset($_GET)) {
	$escapedGet = recursive_escape($_GET);
}




class MyDB extends SQLite3
   {
      function __construct()
      {
         $this->open('dbdata/waitlist.db');
      }
   }
   $db = new MyDB();
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
     // echo "Opened database successfully\n";
   }

//  $db->close(); // auto closes when file is ready. 


function drop_table () {
 global $db; 
   /**************************************
    * Drop previous table                *
    **************************************/

    $sql =<<<EOF
	DROP TABLE IF EXISTS waitlist_user
EOF;
	

 $ret = $db->exec($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
    //  echo "Table dropped successfully\n";
   }

install();
}

//install();
function install() {
 global $db; 
 	
   /**************************************
    * Create tables                       *
    **************************************/
	/* STRUCTURE and comments
		Now, only one table. For easier export and no need for a relational solution atm. 
		Some cons ofcourse, but easier maintenance
		email TEXT NOT NULL, // Primary key gets auto increment
		ref_reg_when DATETIME, // When someone else registered the email
		ref_mail_sentwhen DATETIME, // When someone else made an email go out 
		ref_token TEXT, // Registered Referer
		email_pix_trk DATETIME, // For tracking if recomendation email are opened, when
		email_activate_token TEXT, // To use for email activation (confirming access to mail)
		email_activated_when DATETIME, // When above
		email_activated INTEGER, // Perhaps date should be enough.. but this is for convenience
		self_reg_when DATETIME, // When user did use form for registration
		myown_ref_token TEXT, // The users own referer Token
		name TEXT, // name for greetings etc :)
		ipadr TEXT // IP-adress upon first registration
	*/
	$sql =<<<EOF
		CREATE TABLE IF NOT EXISTS waitlist_user
		(uid INTEGER PRIMARY KEY,
		email TEXT NOT NULL,
		ref_reg_when DATETIME,
		ref_mail_sentwhen DATETIME,
		ref_token TEXT,
		email_opened_pix_trk DATETIME,
		email_activate_token TEXT,
		email_activated_when DATETIME,
		email_activated INTEGER,
		self_reg_when DATETIME,
		myown_ref_token TEXT,
		name TEXT,
		ipadr TEXT);
EOF;
	

 $ret = $db->exec($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
    //  echo "Table created successfully\n";
   }
	return true; //always true ;)
}
		
		
		/*
   
	if(isset($escapedPost['formSendInvitiedemail'])){
		reg_ref_users();
	}
	
	
	*/
	
	
	

//To show all recieved ajax-data. 
//print_r($_POST);
if(isset($_POST['invite_these_emails'])){
	
	//print_r($_SESSION);
	//print_r($_POST);
	//exit();
	reg_ref_users($escapedPost['invite_these_emails']);
}

	
	
	
	function reg_ref_users($invite_these_emails){
		global $db;  //, $escapedPost
		
		#############################################
		#### Register single/multi referal users ####
		#############################################

		// Example array
		// $emailin = "apa1@apa1.se, apa1@apa2.se, apa3@apa1.se, apa4@apa1.se";
		$emailaarray = str_replace(" ","",explode(",", $invite_these_emails));
		$ref_token = $_SESSION['myown_ref_token']; // The one registering the emails!  myown_ref_token becomes the refereeee IE ref_token
		$i = 0;
		$nowymdhis = date("Y-m-d H:i:s");
		//$array_values[] = "";
		foreach($emailaarray as $email){
			// check if valid! 
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
				//check if exists!
				if(get_uid_for_email($email) == ""){ // WOOOOOORKS???????
				// aint existing.. continue! 
				
				//if user activated - send invitation directly! 
				if($_SESSION['email_activated'] == 1){ 
					$ref_mail_sentwhen = $nowymdhis;
					send_invitation_mail($email, $_SESSION['email'], $_SESSION['name']);
					
					// valid
					echo $email . ".. badoooom! Mail på väg. Skicka fler.. :)<br />";
				
				} else {
					//nope we will have to queue up that email for later sending.. 
					echo $email . ":( Aktivera din email tack! Sparar så länge.<br />";

					$ref_mail_sentwhen = "";
				}
				
				// Non-mtimized, here a DBO would be better, since this loop gives extra server load
					$sql =<<<EOF
					INSERT INTO waitlist_user (ref_token,email,ref_reg_when,ref_mail_sentwhen) 
					VALUES ("$ref_token", "$email", "$nowymdhis","$ref_mail_sentwhen");
EOF;
					$ret = $db->exec($sql);
					if(!$ret){
						echo $db->lastErrorMsg();
					} else { //if
						//   echo "INSERT successfully\n";
					}
				} else { //if
					echo $email . " Emailen finns redan registrerad.. <br />";
				} //if end
			} else { //if
				// NOT valid
				echo $email . " Verkar inte vara en korrekt email?<br />";
			} //if end	
		} // foreach
	} // function



//	if(isset( $escapedPost['waitlist']['email'] )){
//		reg_new_user();
//	}

	//if(1){
	function reg_new_user(){
		global $db, $escapedPost, $escapedGet; 

	#############################################
	#### Register one new user               ####
	#############################################

		// check if token available, it stops when it's empty - runs at least once
		do {
			$myown_ref_token = generateRandomString();
			// If you want to test it: 
			// $myown_ref_token = "6c64ew9f07"; @$i++; echo $i; if($i == 10) exit();
		} while ( "" != whostoken( $myown_ref_token ) ); // yes "" is actually faster than '' hmm
		
		$ref_token = $escapedPost['waitlist']['_ref_token'];
		$nowymdhis = date("Y-m-d H:i:s");
		$email_activate_token = generateRandomString(); 
		
		// check if token available, it stops when it's empty - runs at least once
		do {
			$email_activate_token = generateRandomString();
			// If you want to test it: 
			// $myown_ref_token = "6c64ew9f07"; @$i++; echo $i; if($i == 10) exit();
		} while ( "" != whos_email_activate_token( $email_activate_token ) ); // yes "" is actually faster than '' hmm

		
		$name = $escapedPost['waitlist']['name'];
	   	/* to store IP-adr as an int; $ip='192.168.1.10'; ... check ip2long($ip); */
		$ipadr = $_SERVER['REMOTE_ADDR'];
		$email = $escapedPost['waitlist']['email'];

		// Check if already exist - then just update :)


$sql =<<<EOF
	SELECT * FROM waitlist_user where email == "$email" LIMIT 0,1
EOF;
   
	//mysqli_num_rows nope.. 

	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
  //    echo "Table searched successfully\n";

   }
	$row = $ret->fetchArray(SQLITE3_ASSOC);




	if(!isset($row['email'])){
		//first login
		$_SESSION['firstlogin'] = 1; // set welcome salut  
		$sql =<<<EOF
		INSERT INTO waitlist_user ( email, ref_token, email_activate_token, self_reg_when, myown_ref_token, name, ipadr) VALUES ( "$email", "$ref_token", "$email_activate_token", "$nowymdhis", "$myown_ref_token", "$name", "$ipadr" );
EOF;


//currently not handling a error here, unfortunetly
send_activationemail($email, $email_activate_token);


	} else { // still first login! But registered maybe by referal

		if(!isset($row['myown_ref_token'])){
		//first login, but emailed
//		ref_token = "$ref_token", 
		$sql =<<<EOF
		UPDATE waitlist_user SET 
		email_activate_token = "$email_activate_token", 
		self_reg_when = "$nowymdhis", 
		myown_ref_token = "$myown_ref_token", 
		name = "$name", 
		ipadr = "$ipadr" WHERE email="$email"; 
EOF;

//currently not handling a error here, unfortunetly
// perhaps should be activated directly if invited by email(!!) which could prove ownership of email
send_activationemail($email, $email_activate_token);

	} else {
		//not first login, only update ip .. perhaps not so necesary
		// skip updating name(?) .. well hmm. since the field is there . alright then
		$sql =<<<EOF
		UPDATE waitlist_user SET 
		ipadr = "$ipadr", 
		name = "$name" WHERE email="$email"; 
EOF;


		}



		}
	

   $ret = $db->exec($sql);

 


   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
	        echo $db->changes();
	//	echo "<!-- Records created successfully -->\n";

login($email);

	} // if
 		
	} // if




	function send_activationemail($email, $email_activate_token){
		$eresult = mail($email, "Aktiveringsmail ZooZoo..", "
Hejsan, 
För att aktivera din email så är det bara att besöka länken 

http://www.". $_SERVER['HTTP_HOST'] . str_replace("//","/",strtok($_SERVER["REQUEST_URI"],'index'))."/?active_email=". $email_activate_token ."

Någon har registrerat sig med den här emailen på ZooZoo.com Om det inte är du så kan du bortse från emailet. 

Tack!
MVH
ZooZoo.com

");
		return $eresult; 
	}
	

function showtable() { // will mark this up in a real table later
global $db; 
	$sql =<<<EOF
	SELECT * from waitlist_user
EOF;

	$ret = $db->exec($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
		echo "<!-- Records disp successfully -->\n";
	} // if
   
   
	echo "<pre>";
	

   $ret = $db->query($sql);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
		foreach($row as $colname => $col){
			echo $colname . " = " . $col . "<br />";
		}
		echo "<br />";
	}
		echo "</pre>";
		
		
		
	echo "<table>";
	 while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
echo "<tr>";
		foreach($row as $colname => $col){
			
			echo "<td>";
			if($colname == "email_activate_token") echo "Manual email activation; <a href='http://". $_SERVER['HTTP_HOST'] . str_replace("//","/",strtok($_SERVER["REQUEST_URI"],'admin'))."/?active_email=". $col . "' target='_blank'>" ;
			echo $col;
			if($colname == "email_activate_token") echo "<a/> ";
			echo "</td>";
		}
echo "</tr>";
	}
	echo "</table>";
	echo "<!-- Operation done successfully\n -->";
	/* */

}


function extracttoken () {
	//maybe fix ht acess for more good looking tokens like zoozoo.com/ToKen than zoozoo.com?token 
	//echo strpos($_SERVER["REQUEST_URI"],"yolo"); 
	$tokenstart = strpos($_SERVER["REQUEST_URI"],"yolo"); 
	if($tokenstart > -1){
	$tokenend = strpos($_SERVER["REQUEST_URI"],"np", $tokenstart);
	$token = substr($_SERVER["REQUEST_URI"], $tokenstart+4, $tokenend-4-$tokenstart); 
	} else {
		$token = "";
	}
	return $token;
}

function generateRandomString($length = 9)
{
    return substr(sha1(rand()), 0, $length);
}

//whostoken("4dwwwww86ef592");

function whostoken($token)
{
	global $db; 


    $sql =<<<EOF
	SELECT email FROM waitlist_user where myown_ref_token == "$token" LIMIT 0,1
EOF;

	//mysql_num_rows ---> SELECT count(*) FROM ( select users where firstname = 'john' ) 

	// query not exec.. 	   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
    //  echo "Table dropped successfully\n";
   }
   $row = $ret->fetchArray(SQLITE3_ASSOC);

	return $row['email'];
}

function getmyrefurl(){
	$refurl = "http://" . $_SERVER['HTTP_HOST'] . str_replace("??","?",str_replace("//","/",strtok($_SERVER["REQUEST_URI"],'c1'))) . "?yolo" . $_SESSION['myown_ref_token'] . "np"; 
	
	// since the c1 file is part of the request uri.. 
	
	//$refurl = "http://zoozoo.com?yolo" . $_SESSION['myown_ref_token'] . "np"; 
	
	return $refurl;
}


function mysql_real_escape_string_hack($value)
{
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}

function recursive_escape($arrayin) {
// This function is a replacement of sql  prepare stuff with DBO.. not complete but improve some

	if (is_array($arrayin)){
		foreach($arrayin as $key=>$value){
			if (is_array($value)){
				foreach($value as $key2=>$value2){
					$escapedArray[$key][$key2] = mysql_real_escape_string_hack($value2);
				}
			} else {
				$escapedArray[$key] = mysql_real_escape_string_hack($value);
			}
		}
	} else {
		//nothing done
		$escapedArray = $arrayin;
	}
	if(!isset($escapedArray)) {
		$escapedArray = Array ( );
	}
	return $escapedArray;
}



function validateform1(){
	$arrval = array();
	if(filter_var(@$_POST['waitlist']['email'], FILTER_VALIDATE_EMAIL)){
		$arrval['email'] = 0; // valid
	} else {
		$arrval['email'] = 1;
	}
	
	
	
	
	
	if(strlen(@$_POST['waitlist']['name']) > 0) {
		$arrval['name'] = 0; // valid
		//Only letters and space no other stuff.. 
		if (ctype_alpha(str_replace(' ', '', @$_POST['waitlist']['name'])) === false) {
  			$arrval['name'] = 1; // NON valid
		} else {
			// max length 26 letters (to avoid spammers using mail invitation..!)
			if (strlen($arrval['name']) > 26) {
  				$arrval['name'] = 1; // NON valid
			}	
		}
	} else {
		$arrval['name'] = 1;
	}

	return $arrval; 
}


function activate_email($email_activate_token){
	global $db; 
	$email = whos_email_activate_token($email_activate_token);

########wwwwwwwwwwwwww
	$nowymdhis = date("Y-m-d H:i:s");
	$sql =<<<EOF
	UPDATE waitlist_user SET email_activated = "1", email_activated_when = "$nowymdhis" WHERE email_activate_token="$email_activate_token"
EOF;
   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
//Success
   }
   
	$_SESSION['email_activated_now_so_inform'] = 1;		

	login($email);
	
	/*
	// Test logoin data
	echo "<pre>";
	print_r($_SESSION);
	echo "</pre>";
	exit();
	*/
	return true;
	}
	
	
	
	
	function whos_email_activate_token($email_activate_token){
global $db; 
	// Here the actual email adress gets activated
	$sql =<<<EOF
	SELECT email FROM waitlist_user WHERE email_activate_token == "$email_activate_token" LIMIT 0,1
EOF;
   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
   }
	$row = $ret->fetchArray(SQLITE3_ASSOC);

return $row['email'];
}
	
	
	
	
	
	
	
	function login($email){
		global $db; 

$sql =<<<EOF
	SELECT * FROM waitlist_user where email == "$email" LIMIT 0,1
EOF;
   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
  //    echo "Table searched successfully\n";
   }
	$row = $ret->fetchArray(SQLITE3_ASSOC);

		$_SESSION['email'] = $row['email'];
		$_SESSION['name'] =  $row['name'];
		$_SESSION['myown_ref_token'] = $row['myown_ref_token'];
		$_SESSION['email_activate_token'] = $row['email_activate_token'];		
		$_SESSION['email_activated'] = $row['email_activated'];		
	

	} 
	
	
function get_uid_for_email($email){
global $db; 
	// Here the actual email adress gets activated
	$sql =<<<EOF
	SELECT uid FROM waitlist_user WHERE email == "$email" LIMIT 0,1
EOF;
   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
   }
	$row = $ret->fetchArray(SQLITE3_ASSOC);

return $row['uid']; // empty if no match
	}
	
	
	
	function send_invitation_mail($to_email, $from_email, $from_name) { 
		$eresult = mail($to_email, $from_name . " har bjudit in dig till djurens nya favoritplats", "Grattis " . $to_email ."! " . $from_email . " har bjudit in dig till att bli en av de första medlemmarna på nätets nästa stora djurbutik och djurcommunity.

Klicka på länken nedan för att acceptera inbjudan och även ha möjlighet att vinna priser till dig och ditt djur

" . getmyrefurl() . "

Mycket nöje! 

","From: " . $from_email); // 
// Definetly fix: make email activated directly! if clicked... since then we know it's their mail
		return $eresult; 
	}
	
	

	
	// count_my_active_refs();
	function count_my_active_refs() {
		global $db; 

if(isset($_SESSION['myown_ref_token'])) {

$myown_ref_token = $_SESSION['myown_ref_token']; 
//echo $myown_ref_token . "<br />"; 
	// Here's a tough one... the referal must be registerad and activated it's email
	$sql =<<<EOF
	SELECT COUNT(*) as count FROM waitlist_user WHERE ref_token == "$myown_ref_token" AND email_activated = 1
EOF;
   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
   }
	$row = $ret->fetchArray(SQLITE3_ASSOC);

//echo $row['count'];
} else {
echo "myown_ref_token not set! Check code. ";	
exit();

	}
return $row['count'];
		
		}
	
	
	
	
	
	
		// count_my_active_refs();
	function count_my_tot_refs() {
		global $db; 

if(isset($_SESSION['myown_ref_token'])) {

$myown_ref_token = $_SESSION['myown_ref_token']; 
//echo $myown_ref_token . "<br />"; 
	// Here's a tough one... the referal must be registerad and activated it's email
	$sql =<<<EOF
	SELECT COUNT(*) as count FROM waitlist_user WHERE ref_token == "$myown_ref_token"
EOF;
   
	$ret = $db->query($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
   }
	$row = $ret->fetchArray(SQLITE3_ASSOC);

echo $row['count'];
} else {
echo "myown_ref_token not set! Check code. ";	
exit();

	}
return $row['count'];
		
		}
?>